import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.io.FileWriter;   // Import the FileWriter class

public class ContraseņasResumidas {
	public static void main(String[] args){
		createPlainFile();
		createNewPasswords();
	}    
	public static void createPlainFile() {
			BufferedReader reader;
			try {
				File myObj = new File("plain.txt");
				if (myObj.createNewFile()) {
				   System.out.println("File created: " + myObj.getName());
				} else {
				   System.out.println("File already exists.");
				}
				FileWriter myWriter = new FileWriter("plain.txt");
				reader = new BufferedReader(new FileReader(
						"hash-plain.txt"));
				String line = reader.readLine();
				String[] pass;
				while (line != null) {
					if(line.isEmpty()) {
						myWriter.write(line+"\n");
						System.out.println(line);
					}else {
						pass = line.split(":");
						System.out.println(pass[1]);
						myWriter.write(pass[1]+"\n");
					}
					
					// read next line
					line = reader.readLine();
				}
				reader.close();
				myWriter.close();
				System.out.println("Successfully wrote to the file.");
			} catch (IOException e) {
				e.printStackTrace();
			}
	}
	
	public static void createNewPasswords() {
		BufferedReader reader;
		String saltFijo = "MasterDavidRubio";
		try {
			File myObj = new File("new-passwords.txt");
			if (myObj.createNewFile()) {
			   System.out.println("File created: " + myObj.getName());
			} else {
			   System.out.println("File already exists.");
			}
			FileWriter myWriter = new FileWriter("new-passwords.txt");
			reader = new BufferedReader(new FileReader(
					"plain.txt"));
			String line = reader.readLine();
			String saltCompleto;
			String saltVariable;
			String encriptado;
			int i = 0;
			while (line != null) {
				if(line.isEmpty()) {
					myWriter.write(line+"\n");
					System.out.println(line);
				}else {
					saltVariable = "Covid"+String.valueOf(i);
					saltCompleto=saltFijo+saltVariable+line;
					encriptado = cryptWithMD5(saltCompleto);
					System.out.println(encriptado);
					myWriter.write(encriptado+"\n");
				}
				
				// read next line
				i++;
				line = reader.readLine();
			}
			reader.close();
			myWriter.close();
			System.out.println("Successfully wrote to the file.");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private static MessageDigest md;

	   public static String cryptWithMD5(String pass){
	    try {
	        md = MessageDigest.getInstance("MD5");
	        byte[] passBytes = pass.getBytes();
	        md.reset();
	        byte[] digested = md.digest(passBytes);
	        StringBuffer sb = new StringBuffer();
	        for(int i=0;i<digested.length;i++){
	            sb.append(Integer.toHexString(0xff & digested[i]));
	        }
	        return sb.toString();
	    } catch (NoSuchAlgorithmException ex) {
	        ex.printStackTrace();
	    }
	        return null;


	   }
	
}
